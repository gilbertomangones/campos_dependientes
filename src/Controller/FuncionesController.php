<?php
/**
 * @file
 * Contains \Drupal\dependientes\Controller\FuncionesController.
 */
 
namespace Drupal\dependientes\Controller;
 
use Drupal\Core\Controller\ControllerBase;

class FuncionesController extends ControllerBase {
  public function datos_desplegable_dos($id){
  
    // Hacer data del query que obtiene a partir del Id que viene por parámetro
    // que la salida sea asi:
    $output[1] = "ejemplo 1";
    $output[2] = "ejemplo 2";
    $output[3] = "ejemplo 3"; 
    return $output;
  }

}
